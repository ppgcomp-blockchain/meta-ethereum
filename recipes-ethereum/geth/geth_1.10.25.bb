# Copyright (C) 2019 Bernardo Rodrigues <bernardoar@protonmail.com>

SUMMARY = "Official Go implementation of the Ethereum protocol"
DESCRIPTION = "geth is the command line interface for running a full ethereum node implemented in Go."
HOMEPAGE = "https://geth.ethereum.org"
LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-3.0-only;md5=c79ff39f19dfec6d293b95dea7b07891"

SRC_URI = "git://github.com/ethereum/go-ethereum.git;branch=release/1.10;protocol=https"
SRCREV = "d901d85377c2c2f05f09f423c7d739c0feecd90a"

inherit go

GO_IMPORT = "github.com/ethereum/go-ethereum"
GO_INSTALL = "${GO_IMPORT}/cmd/geth"
GO_LINKSHARED = ""
PTEST_ENABLED = "0"

export GO111MODULE="on"

LDFLAGS += "-lpthread"
RDEPENDS:${PN}-dev += " gawk bash"

go_do_compile() {
	export TMPDIR="${GOTMPDIR}"
	${GO} get -d ${GO_LINKSHARED} ${GOBUILDFLAGS} ${GO_IMPORT}
	${GO} install ${GO_LINKSHARED} ${GOBUILDFLAGS} ${GO_INSTALL}@v${PV}
	echo "INSTALLED"
	chmod u+w "${GOPATH}" -R
	echo "${PN}"
}
