# Copyright (C) 2022 Maykon Valério da Silva <maykonvs@gmail.com>

SUMMARY = "Framework for benchmarking private blockchain systems"
DESCRIPTION = "BlockBench is the first benchmarking framework for private blockchain systems. \
It serves as a fair means of comparison for different platforms and enables deeper understanding \
of different system design choices"
HOMEPAGE = "https://gitlab.com/ppgcomp-blockchain/blockbench"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/git/LICENSE;md5=5d1456c70acf48526e705d14b2f4f0f2"

SRC_URI = "git://gitlab.com/ppgcomp-blockchain/blockbench.git;branch=docker-ssh;protocol=https"
SRCREV = "636d9dac7fcec4d049fab6bcc3a6b34cd0c92730"

S = "${WORKDIR}/git"
MY_DESTINATION = "/workspaces/blockbench"

FILES:${PN} += "${MY_DESTINATION}"

RDEPENDS:${PN} += " perl bash"

do_install() {
    install -d ${D}${MY_DESTINATION}
    cp -rT ${S} ${D}${MY_DESTINATION}
}
